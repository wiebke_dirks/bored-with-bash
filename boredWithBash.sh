#!/bin/bash

n=1

function print_help {
 printf "Options:\n
      -p : number of participants, defaults to random\n
      -t : type of actity, defaults to random.\n
           valid types: education, recreational, social, diy, charity, cooking, relaxation, music, busywork\n
      -n : number of suggestions, defaults to 1\n" >&2 ;
 }

while getopts p:t:n:h opt; do
    case "$opt" in
        h)
            print_help; exit 1;;
        t)
            type=${OPTARG}
            ;;

        p)
            participants=${OPTARG}
            ;;

        n)
            n=${OPTARG}
            ;;
        *)
            echo "Invalid option(s)" >&2
            exit 3
            ;;
    esac
done

QUERY="http://www.boredapi.com/api/activity/"

if [[ -n "${participants}" && -n "${type}" ]]; then
  QUERY+="?type=${type}&participants=${participants}"
elif [[ -n "${type}" ]]; then
  QUERY+="?type=${type}"
elif [[ -n "${participants}" ]]; then
  QUERY+="?participants=${participants}"
fi

COUNTER=1
  while [ $COUNTER -le $n ]; do
    RESULT+="\nActivity suggestion number $COUNTER\n"
    RESULT+=$(curl -s -w "\n" "${QUERY}" | jq .)
    let COUNTER=COUNTER+1
  done

printf "${RESULT}"


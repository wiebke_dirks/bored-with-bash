Bored with Bash

This is a rather uneccessary script to query the bored API (www.boredapi.com).

To run the script, download it, open a terminal and type:
``./boredWithBash.sh``

This will return a random acitivity suggestion.

You can supply the following command line arguments to filter through the activities:

```
Options:

      -p : number of participants, defaults to random

      -t : type of actity, defaults to random.

           valid types: education, recreational, social, diy, charity, cooking, relaxation, music, busywork

      -n : number of suggestions, defaults to 1
```

You can use ``-h`` to see the available options.

Example input:

``./boredWithBash.sh -p 3 -t social -n 3``

Example output:
```
Activity suggestion number 1
{
  "activity": "Go on a fishing trip with some friends",
  "type": "social",
  "participants": 3,
  "price": 0.4,
  "link": "",
  "key": "3149232",
  "accessibility": 0.4
}
Activity suggestion number 2
{
  "activity": "Host a movie marathon with some friends",
  "type": "social",
  "participants": 3,
  "price": 0.1,
  "link": "",
  "key": "5914292",
  "accessibility": 0
}
Activity suggestion number 3
{
  "activity": "Go on a fishing trip with some friends",
  "type": "social",
  "participants": 3,
  "price": 0.4,
  "link": "",
  "key": "3149232",
  "accessibility": 0.4
```
